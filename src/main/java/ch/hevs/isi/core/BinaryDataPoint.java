package ch.hevs.isi.core;

/**
 * The BinaryDataPoint class if there are binary DataPoints used
 */
public class BinaryDataPoint extends DataPoint {
    /**
     *
     */
    private boolean value;

    /**
     * Constructor of binary DataPoint
     * @param label the name of the DataPoint
     * @param isOutput if the DataPoint is an input (0) or an output (1)
     */
    public BinaryDataPoint(String label, boolean isOutput){
        super(label, isOutput);
    }

    /**
     * Save the current value
     * @return the value in a String
     */
    public String getValue() {
        return value ? "true":"false";
    }

    /**
     * Write the new value in the DataBase
     * @param value the value to the DataPoint
     */
    public void setValue(boolean value) {
        this.value = value;
        publish();
    }

    public void setValue(String value) {
        setValue(Boolean.parseBoolean(value));
    }
}
