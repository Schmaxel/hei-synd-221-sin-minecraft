package ch.hevs.isi.core;

/**
 * The FloatDataPoint class if there are float DataPoints used
 */
public class FloatDataPoint extends DataPoint {

    private float value;

    /**
     * Constructor of float DataPoint
     * @param label the name of the DataPoint
     * @param isOutput if the DataPoint is an input (0) or an output (1)
     */
    public FloatDataPoint(String label, boolean isOutput){
        super(label, isOutput);
    }

    /**
     * Save the current value
     * @return the value in a String
     */
    public String getValue() {
        return String.valueOf(value);
    }

    /**
     * Write the new value in the DataBase
     * @param value the value to the DataPoint
     */
    public void setValue(float value) {
        this.value = value;
        publish();
    }

    public void setValue(String value) {
        float val = Float.parseFloat(value);
        setValue(val);
    }
}




