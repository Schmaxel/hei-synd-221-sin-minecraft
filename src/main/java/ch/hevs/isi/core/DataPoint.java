package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.util.HashMap;

/**
 * The DataPoint is use to transfer different DataPoints
 */
public abstract class DataPoint {

    private final static HashMap<String, DataPoint> hm = new HashMap<>();
    private String label;
    private boolean isOutput;

    /**
     * Constructor
     * @param label the name of the DataPoint
     * @param isOutput if the DataPoint is an input (0) or an output (1)
     */
    protected DataPoint(String label, boolean isOutput) {
        this.label = label;
        this.isOutput = isOutput;
        hm.put(label,this);
    }

    /**
     * Save the current label
     * @return the value in a String
     */
    public String getLabel() {
        return label;
    }

    /**
     * Save the current value /abstract
     * @return the value in a String
     */
    public abstract String getValue();

    public abstract void setValue(String value);

    /**
     * Select if input or output
     * @return  if the DataPoint is an input (0) or an output (1)
     */
    public boolean isOutput() {
        return isOutput;
    }

    /**
     * Saves the DataPoint in a HashMap
     * @param label the name of the DataPoint
     * @return the label in a HashMap
     */
    public static DataPoint getDataPointFromLabel(String label) {
        return hm.get(label);
    }

    /**
     *
     */
    protected void publish() {

        DatabaseConnector dbc = DatabaseConnector.getInstance();
        dbc.onNewValue(this);

        WebConnector wbc = WebConnector.getInstance();
        wbc.onNewValue(this);

        FieldConnector fdc = FieldConnector.getInstance();
        fdc.onNewValue(this);
    }
}