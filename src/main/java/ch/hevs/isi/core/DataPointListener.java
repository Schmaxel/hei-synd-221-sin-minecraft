package ch.hevs.isi.core;

import java.io.IOException;

/**
 * The DataPointListener Interface is for the DataPoint class
 */
public interface DataPointListener {
    /**
     * Absract method that is used to merge the float and binary DataPoints.
     * @param dp the DataPoint which can be float or boolean
     */
    void onNewValue (DataPoint dp) throws IOException;
}
