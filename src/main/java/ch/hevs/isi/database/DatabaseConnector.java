package ch.hevs.isi.database;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.utils.Utility;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;

import static ch.hevs.isi.utils.Utility.DEBUG;

/**
 * The DataBaseConnector class is used to send/receive Data (float or boolean) to the DataBase.
 *
 */
public class DatabaseConnector implements DataPointListener {

    /**
     * Creates Connector to the DataBase.
     */
    private static DatabaseConnector dbc = null;
    private static String PASS;
    private static String USER;
    private static String DB;
    private static String URL;


    private DatabaseConnector() {
    }

    /**
     * Controls if the Connector is the only one.
     *
     * @return An unique DataBase
     */
    public static DatabaseConnector getInstance() {
        if (dbc == null) {
            dbc = new DatabaseConnector();
        }
        return dbc;
    }
    public static void config(String url, int port, String db, String user, String pass){

    URL = url;
    DB = db;
    USER = user;
    PASS = pass;
    }

    /**
     * Sends the label and value to the DataBase
     *
     * @param label The name of the DataPoint
     * @param value The value of the DataPoint
     */
    private void pushToDataBase(String label, String value){

        final String request = "https://" + URL +"/write?db="+DB;
        final String userpass = DB + ":"+ PASS;
        try {

        URL url = new URL(request);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());

        conn.setRequestProperty("Authorization", "Basic " + encoding);
        conn.setRequestProperty("Content-type", "binary/octet-stream");

        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

         OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
         writer.write(label + " value=" + value);
         writer.flush();
         int responseCode = conn.getResponseCode();
        //System.out.println(responseCode);

         BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
         if (responseCode == 204) {
             while ((in.readLine()) != null) {
                }
            }
            conn.disconnect();
        }
        catch (ProtocolException e) {
            DEBUG("DataBaseConnector", "pushToDataBase()", "Protocol Exception : " + e.getMessage());
        } catch (MalformedURLException e) {
            DEBUG("DataBaseConnector", "pushToDataBase()", "Malform Exception : " + e.getMessage());
        } catch (IOException e) {
            DEBUG("DataBaseConnector", "pushToDataBase()", "IO Exception : " + e.getMessage());
        }


    }

    /**
     * Send the new value of the DataPoint to the DataBase
     * @param dp is a message that be use to send value in different location
     */
    @Override
    public void onNewValue(DataPoint dp) {
        pushToDataBase(dp.getLabel(), dp.getValue());
    }

    // For the test
    public static void main(String[] args){
        config("https://influx.sdi.hevs.ch",2222,"SIn17","SIn17","5bb931dd5d6e0d127c11a7de1e884ad6");
        DatabaseConnector db = DatabaseConnector.getInstance();
        while (true) {
            db.pushToDataBase("GRID_U_FLOAT", "700");
            Utility.waitSomeTime(1000);
            db.pushToDataBase("GRID_U_FLOAT", "900");
            Utility.waitSomeTime(1000);
            db.pushToDataBase("GRID_U_FLOAT", "1000");
            Utility.waitSomeTime(1000);
            db.pushToDataBase("GRID_U_FLOAT", "900");
            Utility.waitSomeTime(1000);
            db.pushToDataBase("GRID_U_FLOAT", "700");

        }
    }
}