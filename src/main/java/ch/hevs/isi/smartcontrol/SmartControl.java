package ch.hevs.isi.smartcontrol;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.ModbusAccessor;
import jdk.nashorn.internal.runtime.regexp.JoniRegExp;

import java.sql.Time;
import java.time.Clock;
import java.util.Timer;
import java.util.TimerTask;

public class SmartControl {
    private static SmartControl smc  = null;
    private Timer smartTimer = null;
    private TimerTask smartTask = new TimerTask() {
        @Override
        public void run() {
            smartcontrolling();
            System.out.println("Contrôle en cours");
        }
    };
    public SmartControl(){
        smartTimer = new Timer();
    }
    public static SmartControl getInstance(){
        if ( smc == null) {
            smc = new SmartControl();
        }
        return smc;
    }
    // Variables for easier control, with easier names
    DataPoint GridVoltage= DataPoint.getDataPointFromLabel("GRID_U_FLOAT");
    DataPoint BatteryPowerUse = DataPoint.getDataPointFromLabel("BATT_P_FLOAT");
    DataPoint BatteryPowerLeft = DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
    DataPoint SolarPower = DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
    DataPoint WindPower = DataPoint.getDataPointFromLabel("WIND_P_FLOAT");

    DataPoint CoalPower = DataPoint.getDataPointFromLabel("COAL_P_FLOAT");
    DataPoint CoalAmount = DataPoint.getDataPointFromLabel("COAL_AMOUNT");
    DataPoint HomePowerUse = DataPoint.getDataPointFromLabel("HOME_P_FLOAT");
    DataPoint PublicPowerUse = DataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
    DataPoint FactoryPowerUse = DataPoint.getDataPointFromLabel("FACTORY_P_FLOAT");

    DataPoint BunkerPowerUse = DataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
    DataPoint WindStrength = DataPoint.getDataPointFromLabel("WIND_FLOAT");
    DataPoint WeatherQuality = DataPoint.getDataPointFromLabel("WEATHER_FLOAT");
    DataPoint WeatherForecast = DataPoint.getDataPointFromLabel("WEATHER_FORECAST_FLOAT");
    DataPoint WeatherCountdown = DataPoint.getDataPointFromLabel("WETHER_COUNTDOWN_FLOAT");

    DataPoint TimeOfDay = DataPoint.getDataPointFromLabel("CLOCK_FLOAT");
    DataPoint RemoteCoalSetPoint = DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
    DataPoint RemoteFactorySetPoint = DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
    DataPoint RemoteSolarSwitch = DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
    DataPoint RemoteWindSwitch = DataPoint.getDataPointFromLabel("REMOTE_WIND_SW");

    DataPoint FactoryEnergyConsumed = DataPoint.getDataPointFromLabel("FACTORY_ENERGY");
    DataPoint Score = DataPoint.getDataPointFromLabel("SCORE");
    DataPoint CoalStatus = DataPoint.getDataPointFromLabel("COAL_ST");
    DataPoint FactoryStatus= DataPoint.getDataPointFromLabel("FACTORY_ST");
    DataPoint SolarStatus = DataPoint.getDataPointFromLabel("SOLAR_CONNECT_ST");

    DataPoint WindStatus = DataPoint.getDataPointFromLabel("WIND_CONNECT_ST");
/*

        public void smartcontrolling(){

            RemoteSolarSwitch.setValue("true");
            RemoteWindSwitch.setValue("true");
            if(Float.valueOf(GridVoltage.getValue())< 800f){
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue())+0.05f));
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())-0.01f));
                System.out.println("Coal+0.05 et Factory -0.01");
            }
            else if (Float.valueOf(GridVoltage.getValue())> 800f){
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(GridVoltage.getValue())-0.05f));
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())+0.01f));
                System.out.println("Coal -0.05 et Factory +0.01");
            }
            else {}
            if(Float.valueOf(TimeOfDay.getValue()) == 0.75f){
                RemoteCoalSetPoint.setValue("0.5f");
            }

            String timeOfDay = DataPoint.getDataPointFromLabel("CLOCK_FLOAT").getValue();
            if(Float.valueOf(timeOfDay)>0.5){
                System.out.println("Its day");
            }
            if(Float.valueOf(timeOfDay)<0.5){
            }

        }
        */

    /**
     * Smartcontrolling is the regulation of the Minecraft world to get the most efficient Factory. We want the Factory to be ON as much as possible
     */
    public void smartcontrolling(){
        System.out.println("Score :" + Score.getValue());
        RemoteSolarSwitch.setValue("true");
        RemoteWindSwitch.setValue("true");
if(Float.valueOf(RemoteFactorySetPoint.getValue())>=1.0f){
    RemoteFactorySetPoint.setValue("1.0f");
}
if(Float.valueOf(RemoteFactorySetPoint.getValue())<=0.0f){
            RemoteFactorySetPoint.setValue("0.0f");
        }
if(Float.valueOf(RemoteCoalSetPoint.getValue())>=1.0f){
    RemoteCoalSetPoint.setValue("1.0f");
}
if(Float.valueOf(RemoteCoalSetPoint.getValue())<=0.0f){
            RemoteCoalSetPoint.setValue("0.0f");
        }
if(Float.valueOf(BatteryPowerLeft.getValue())<0.1f){
    RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())-0.1f));
}

if(Float.valueOf(BatteryPowerLeft.getValue())>0.9f){
    RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())+0.1f));
        }

if(Float.valueOf(TimeOfDay.getValue())>0f && Float.valueOf(TimeOfDay.getValue()) <0.25f){
            //Morning, time slot morning where we control the factory and coal production
            // System.out.println("Morning");
            if(Float.valueOf(BatteryPowerLeft.getValue())<0.4f){
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())-0.01f));
                RemoteCoalSetPoint.setValue(String.valueOf((Float.valueOf(RemoteCoalSetPoint.getValue())+0.02f)));
            }
            else {
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue()) + 0.01f));
                RemoteCoalSetPoint.setValue(String.valueOf((Float.valueOf(RemoteCoalSetPoint.getValue())-0.01f)));
            }
        }
        else if(Float.valueOf(TimeOfDay.getValue())>0.25f && Float.valueOf(TimeOfDay.getValue()) <0.5f){
            // Day, time slot day where we control the factory and coal production
            // System.out.println("Day");
            if(Float.valueOf(BatteryPowerLeft.getValue())<0.35f){
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())-0.01f));
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue())+0.01f));
            }
            else {
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue()) + 0.03f));
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue())-0.03f));
            }
            }
        else if(Float.valueOf(TimeOfDay.getValue())>0.5f && Float.valueOf(TimeOfDay.getValue()) <0.75f){
            //Evening, time slot evening where we control the factory and coal production
            //System.out.println("Evening");
            if(Float.valueOf(BatteryPowerLeft.getValue())<0.6f){
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())-0.02f));
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue())+0.02f));
            }
            else {
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue()) + 0.01f));
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue())-0.01f));
            }
        }
        else if(Float.valueOf(TimeOfDay.getValue())>0.75f && Float.valueOf(TimeOfDay.getValue()) <0.99f){
            //Night, time slot night where we control the factory and coal production
            //System.out.println("Night");
            if(Float.valueOf(BatteryPowerLeft.getValue())<=0.25f){
                RemoteFactorySetPoint.setValue("0.0f");
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue()) + 0.04f));
            }
            if(Float.valueOf(BatteryPowerLeft.getValue())<0.6f && Float.valueOf(BatteryPowerLeft.getValue())>0.2f){
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())-0.03f));
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue())+ 0.02f));
            }
            else
                RemoteFactorySetPoint.setValue(String.valueOf(Float.valueOf(RemoteFactorySetPoint.getValue())+0.01f));
                RemoteCoalSetPoint.setValue(String.valueOf(Float.valueOf(RemoteCoalSetPoint.getValue()) - 0.01f));
        }
    }

    /**
     * Timer to send and receive informations for the control of the Minecraft world
     * @param period time interval of the control
     */
    public void startTimerController(int period) {
        smartTimer.scheduleAtFixedRate(smartTask, 0, period);
    }
}
