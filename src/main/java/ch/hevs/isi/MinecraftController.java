package ch.hevs.isi;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.*;
import ch.hevs.isi.smartcontrol.SmartControl;
import ch.hevs.isi.utils.Utility;
import org.w3c.dom.ranges.Range;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class MinecraftController {
    public static boolean USE_MODBUS4J = false;

    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) {

        String dbHostName    = "localhost";
        String dbName        = "labo";
        String dbUserName    = "root";
        String dbPassword    = "root";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }
        DatabaseConnector.config(dbHostName,modbusTcpPort,dbName,dbUserName,dbPassword);
        FieldConnector.configuration(modbusTcpHost,modbusTcpPort);
        FloatRegister.configuration(modbusTcpHost,modbusTcpPort);
        BooleanRegister.configuration(modbusTcpHost,modbusTcpPort);

        String filePath = "ressources\\ModbusEA_SIn.csv";


        try {
            BufferedReader bf = new BufferedReader(new FileReader(filePath));
            ArrayList < String > Label_al = new ArrayList < String > ();
            ArrayList < String > Unit_al = new ArrayList < String > ();
            ArrayList < String > Description_al = new ArrayList < String > ();
            ArrayList < String > Output_al = new ArrayList< String >();
            ArrayList < String > Adress_al = new ArrayList < String > ();
            ArrayList < String > Range_al = new ArrayList < String > ();
            ArrayList < String > Offset_al = new ArrayList< String >();
            while (bf.ready()) {
                String[] c = bf.readLine().split(";");
                Label_al.add(c[0]);
                Unit_al.add(c[1]);
                Description_al.add(c[8]);
                Output_al.add(c[4]);
                Adress_al.add(c[5]);
                Range_al.add(c[6]);
                Offset_al.add(c[7]);
            }
            String[] Label_list = Label_al.stream().toArray(String[]::new);
            String[] Unit_list = Unit_al.stream().toArray(String[]::new);
            String[] Description_list = Description_al.stream().toArray(String[]::new);
            String[] Output_list = Output_al.stream().toArray(String[]::new);
            String[] Adress_list = Adress_al.stream().toArray(String[]::new);
            String[] Range_list = Range_al.stream().toArray(String[]::new);
            String[] Offset_list = Offset_al.stream().toArray(String[]::new);
            for(int i = 1; i <= Label_list.length-1 ; i++)
            {
                FieldConnector.getInstance().addModbusRegister(Label_list[i],Boolean.valueOf(Output_list[i]), Integer.valueOf(Adress_list[i]), Integer.valueOf(Range_list[i]),Integer.valueOf(Offset_list[i]),Boolean.valueOf(Description_list[i]));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        SmartControl sc = new SmartControl();
        sc.smartcontrolling();
/*
        FieldConnector.getInstance().addModbusRegister("GRID_U_FLOAT",false,89,1000,0,false);
        FieldConnector.getInstance().addModbusRegister("BATT_P_FLOAT",false,57,6000,-3000,false);
        FieldConnector.getInstance().addModbusRegister("BATT_CHRG_FLOAT",false,49,1,0,false);
        FieldConnector.getInstance().addModbusRegister("SOLAR_P_FLOAT",false,61,1500,0,false);
        FieldConnector.getInstance().addModbusRegister("WIND_P_FLOAT",false,53,1000,0,false);

        FieldConnector.getInstance().addModbusRegister("COAL_P_FLOAT",false,81,600,0,false);
        FieldConnector.getInstance().addModbusRegister("COAL_AMOUNT",false,65,1,0,false);
        FieldConnector.getInstance().addModbusRegister("HOME_P_FLOAT",false,101,1000,0,false);
        FieldConnector.getInstance().addModbusRegister("PUBLIC_P_FLOAT",false,97,500,0,false);
        FieldConnector.getInstance().addModbusRegister("FACTORY_P_FLOAT",false,105,2000,0,false);

        FieldConnector.getInstance().addModbusRegister("BUNKER_P_FLOAT",false,93,500,0,false);
        FieldConnector.getInstance().addModbusRegister("WIND_FLOAT",false,301,1,0,false);
        FieldConnector.getInstance().addModbusRegister("WEATHER_FLOAT",false,305,1,0,false);
        FieldConnector.getInstance().addModbusRegister("WEATHER_FORECAST_FLOAT",false,309,1,0,false);
        FieldConnector.getInstance().addModbusRegister("WEATHER_COUNTDOWN_FLOAT",false,313,600,0,false);

        FieldConnector.getInstance().addModbusRegister("CLOCK_FLOAT",false,317,1,0,false);
        FieldConnector.getInstance().addModbusRegister("REMOTE_COAL_SP",true,209,1,0,false);
        FieldConnector.getInstance().addModbusRegister("REMOTE_FACTORY_SP",true,205,1,0,false);
        FieldConnector.getInstance().addModbusRegister("REMOTE_SOLAR_SW",true,401,1,0,true);
        FieldConnector.getInstance().addModbusRegister("REMOTE_WIND_SW",true,405,1,0,true);

        FieldConnector.getInstance().addModbusRegister("FACTORY_ENERGY ",false,341,3600000,0,false);
        FieldConnector.getInstance().addModbusRegister("SCORE ",false,345,3600000,0,false);
        FieldConnector.getInstance().addModbusRegister("COAL_ST ",false,601,1,0,false);
        FieldConnector.getInstance().addModbusRegister("FACTORY_ST ",false,605,1,0,false);
        FieldConnector.getInstance().addModbusRegister("SOLAR_CONNECT_ST",false,609,1,0,true);
        FieldConnector.getInstance().addModbusRegister("WIND_CONNECT_ST",false,613,1,0,true);
*/
        // Start polling Minecraft's registers
        ModbusAccessor.getInstance().writeFloat(205,0.2f);
        ModbusAccessor.getInstance().writeFloat(209,0.2f);
        FieldConnector.getInstance().startPolling(2500);
        SmartControl.getInstance().startTimerController(2500);
    }
}
