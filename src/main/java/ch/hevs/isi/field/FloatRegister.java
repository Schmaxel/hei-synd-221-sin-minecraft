package ch.hevs.isi.field;

import ch.hevs.isi.core.FloatDataPoint;

/**
 * There are two types of register. This one controls the register with float values.
 */
public class FloatRegister extends ModbusRegister {
private int range;
private int offset;
private FloatDataPoint dataPoint;

    /**
     * Class used if the register we are trying to process is a float value
     * @param isOutput  If the varible is used as input or output
     * @param address Value of the register we are using
     * @param label Name of the register
     * @param range Range of values we want as Input/Output
     * @param offset Offsets the value we have by this amount
     */
    public FloatRegister(boolean isOutput, int address, String label, int range,int offset) {
    super(address, false);
    this.range = range;
    this.offset = offset;
    dataPoint = new FloatDataPoint(label,isOutput);
    this.addModbusRegister(dataPoint);
}
    static String modbusHost;
    static int modbusPort;
    /**
     * Configuration of the modbus parameters
     * @param modbusTcpHost IP addresse of the field/Minecraft world
     * @param modbusTcpPort Port of the field/Minecraft world
     */

    public static void configuration(String modbusTcpHost,int modbusTcpPort){
        modbusHost = modbusTcpHost;
        modbusPort = modbusTcpPort;
    }

    /**
     * Read the value of the register we want with the location of address, with offset and range
     */
   public void read(){
        dataPoint.setValue(ModbusAccessor.getInstance(modbusHost,modbusPort).readFloat(getAddress())*range + offset);
    }

    /**
     * Write the value in the register we want, with offset and range
     */
   public void write(){
        ModbusAccessor.getInstance(modbusHost,modbusPort).writeFloat(getAddress(),((Float.valueOf(dataPoint.getValue()))-offset)/range);
     }
}
