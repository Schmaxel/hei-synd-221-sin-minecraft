package ch.hevs.isi.field;
import ch.hevs.isi.core.DataPoint;
import java.util.*;

/**
 * Keeps track of the labels that are linked with number
 */
public abstract class ModbusRegister {
    private int address;
    private boolean isBinary;
    private static final Map<DataPoint, ModbusRegister> registersMap = new HashMap<>();

    /**
     *
     * @param address Value of the register we are using
     * @param isBinary If the value is binary, we will send the values to the BooleanRegister. If the value is float, we will send the values to the FLoatRegister
     */
    public ModbusRegister(int address, boolean isBinary) {
        this.address = address;
        this.isBinary = isBinary;
    }

    /**
     * Getter of the Register to know if the value is binare and the address for the location
     * @return
     */
    public boolean isBinary()   { return isBinary; }
    public int getAddress()     { return address; }

    /**
     * abstract read and write method for the Register
     */
    public abstract void read();
    /**
     * abstract read and write method for the Register
     */
    public abstract void write();

    /**
     * Add a new register to our Map
     * @param dp Give the datapoint we want
     */
    protected void addModbusRegister(DataPoint dp) {
        registersMap.put(dp, this);
    }

    /**
     * Give the register for a Datapoint we want
     * @param dp DataPoint we want to check
     * @return
     */
    public static ModbusRegister getRegisterFromDataPoint(DataPoint dp) {
        return registersMap.get(dp);
    }

    /**
     * Get the number of registers we have saved
     * @return
     */
    public static int getNumberOfRegister() {
        return registersMap.size();
    }

    /**
     * Gives back all the registers
     * @return
     */
    public static Collection<ModbusRegister> getRegisters() {
        return registersMap.values();
    }
}
