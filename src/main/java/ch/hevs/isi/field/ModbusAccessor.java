package ch.hevs.isi.field;
import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

import javax.rmi.CORBA.Util;

/**
 * Class that connects the componets together
 */
public class ModbusAccessor {

    ModbusFactory mbFactory;
    IpParameters ipParam;
    ModbusMaster mbMaster;

    private static ModbusAccessor instance = null;

    /**
     * Function that enables the communication between the game and the user
     * @param host Ip addresse of the installation we want to receive or send data from
     * @param port Port of the application
     */
    private ModbusAccessor(String host, int port) {
        mbFactory = new ModbusFactory();
        ipParam = new IpParameters();
        ipParam.setPort(port);
        ipParam.setHost(host);
        mbMaster = mbFactory.createTcpMaster(ipParam, false);
        try {
            mbMaster.init();
        } catch (Exception e) {
            System.out.println("Modbus Master init Error :" + e.getMessage());
            return;
        }
    }

    /**
     * Class with fixed ip addresse and port. This can work in our conditions, but if you change the ip addresse or the port this will not adapt
     * @return returns the informations of the old or new class
     */
    public static ModbusAccessor getInstance() {
        if (instance == null) {
            instance = new ModbusAccessor("localhost", 1502);
        }
        return instance;
    }

    /**
     * Class that accepts all hosts or ports options
     * @param host Ip addresse of the installation we want to receive or send data from
     * @param port Port of the application
     * @return returns the information of the old or new class
     */
    public static ModbusAccessor getInstance(String host, int port) {
        if (instance == null) {
            instance = new ModbusAccessor(host, port);
        }
        return instance;
    }

    /**
     * Reads a float value in some register.
     * @param regAddress location of the wanted information
     * @return returns the float value of the wanted register
     */
    public Float readFloat(int regAddress) {

        try {
            return (Float) mbMaster.getValue(BaseLocator.inputRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT));
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Reads a boolean value in some register.
     * @param regAddress location of the wanted information
     * @return returns the boolean value of the wanted register
     */
    public boolean readBoolean(int regAddress) {
        try {
            return mbMaster.getValue(BaseLocator.coilStatus(1, regAddress));
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Write some value in the wanted register
     * @param regAddress Location of the register
     * @param newValue value you want the register to have
     * @return
     */
    public boolean writeFloat(int regAddress, float newValue) {

        try {
            mbMaster.setValue(BaseLocator.holdingRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT), newValue);
            return true;
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Write some value in the wanted register
     * @param regAddress Location of the register
     * @param newValue value you want the register to have
     * @return
     */
    public boolean writeBoolean(int regAddress, boolean newValue) {
        try {
            mbMaster.setValue(BaseLocator.coilStatus(1, regAddress), newValue);
            return true;
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * main that lets you test the class ModbusAccessor
     * @param args -
     */

    public static void main(String[] args) {
        ModbusAccessor MbAccess = ModbusAccessor.getInstance("localhost", 1502);
        boolean running = true;
        while (running) {
            Boolean value = MbAccess.readBoolean(609);
            if( value == null ){
                Utility.DEBUG("ModbusAccessor", "main", "Modbus connection error");
                running =false;
            }
            else{
                Utility.DEBUG("ModbusAccessor","main","Registre 609 :" + value);
            }
            Utility.waitSomeTime(1000);
            MbAccess.writeBoolean(401,!value);
            float valueCoal = MbAccess.readFloat(601);
            Utility.DEBUG("ModbusAccessor","main","Registre65 :" + valueCoal);
            MbAccess.writeFloat(209,0.4f);
        }
    }

}
