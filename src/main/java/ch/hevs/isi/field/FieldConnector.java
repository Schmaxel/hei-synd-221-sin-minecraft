package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;


import java.util.Timer;
import java.util.TimerTask;

/**
 * The FieldConnector class is used to send/receive Data (float or boolean) to the Field.
 */
public class FieldConnector implements DataPointListener {
    /**
     * Creates Connector to the Field.
     */
    private static FieldConnector fdc = null;
    private Timer pollTimer = null;

    private TimerTask pollTask = new TimerTask() {
        @Override
        public void run() {
            poll();
        }
    };

    private FieldConnector(){
//       long period = 5000;
       pollTimer = new Timer();
//       pollTimer.scheduleAtFixedRate(new PollTask(),0,period);
    }
    /**
     * Controls if he Connector is the only one.
     * @return An unique DataBase
     */
    static String modbusHost;
    static int modbusPort;

    public static void configuration(String modbusTcpHost,int modbusTcpPort){
        modbusHost = modbusTcpHost;
        modbusPort = modbusTcpPort;
    }
        public static FieldConnector getInstance(){
            if ( fdc == null) {
                fdc = new FieldConnector();
            }
            return fdc;
        }

    /**
     * Send the new value of the DataPoint to the Field
     * @param dp
     */
    @Override
    public void onNewValue(DataPoint dp) {
      //  System.out.println("pushToField: " + dp.getLabel()+ " value="+dp.getValue());
        ModbusRegister mr = ModbusRegister.getRegisterFromDataPoint(dp);

        if (mr != null) {
            if (mr.isBinary())
                ModbusAccessor.getInstance(modbusHost,modbusPort).writeBoolean(mr.getAddress(), Boolean.parseBoolean(dp.getValue()));
            else
                ModbusAccessor.getInstance(modbusHost,modbusPort).writeFloat(mr.getAddress(), Float.parseFloat(dp.getValue()));
        }
    }

    public void addModbusRegister(String label, boolean isOutput, int address, int range, int offset, boolean isBinary) {
        if (isBinary) {
            new BooleanRegister(isOutput, address, label);
        } else {
            new FloatRegister(isOutput, address, label, range, offset);
        }
    }

    public void startPolling(int period) {
        pollTimer.scheduleAtFixedRate(pollTask, 0, period);
    }

    private void poll() {
        for (ModbusRegister mr: ModbusRegister.getRegisters()) {
            mr.read();
        }
    }
}


