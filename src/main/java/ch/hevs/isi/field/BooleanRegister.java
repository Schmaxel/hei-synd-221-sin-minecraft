package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;

/**
 * There are two types of register. This one controls the register with boolean values.
 */
public class BooleanRegister extends ModbusRegister {
    private BinaryDataPoint dataPoint;

    /**
     * Class used if the register we are trying to process is a boolean value
     * @param isOutput If the varible is used as input or output
     * @param address Value of the register we are using
     * @param label Name of the register
     */
    public BooleanRegister(boolean isOutput, int address, String label) {
        super(address, true);
        dataPoint = new BinaryDataPoint(label, isOutput);
        this.addModbusRegister(dataPoint);
    }

    /**
     * Read the value of the register we want with the location of address
     */
    public void read() {
        dataPoint.setValue(ModbusAccessor.getInstance(modbusHost,modbusPort).readBoolean(getAddress()));
    }
    static String modbusHost;
    static int modbusPort;

    /**
     * Configuration of the modbus parameters
     * @param modbusTcpHost IP addresse of the field/Minecraft world
     * @param modbusTcpPort Port of the field/Minecraft world
     */
    public static void configuration(String modbusTcpHost,int modbusTcpPort){
        modbusHost = modbusTcpHost;
        modbusPort = modbusTcpPort;
    }

    /**
     * Write the value in the register we want
     */
    public void write() {
        ModbusAccessor.getInstance(modbusHost,modbusPort).writeBoolean(getAddress(), Boolean.valueOf(dataPoint.getValue()));
    }

}
