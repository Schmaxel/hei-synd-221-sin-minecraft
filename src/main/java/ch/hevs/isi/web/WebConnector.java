package ch.hevs.isi.web;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;
import java.util.Vector;

/**
 * The WebConnector class is used to send/receive Data (float or boolean) to the Web.
 */
public class WebConnector extends WebSocketServer implements DataPointListener {
    Vector<WebSocket> v = new Vector<>();

    /**
     * Creates Connector to the Web.
     */
    private static WebConnector wbc = null;

    /**
     * Constructor
     */
    private WebConnector() {
        super(new InetSocketAddress(8888));
        start();
    }

    /**
     * That function is used to close a WebSocket in case of the operator quit the webpage.
     * @param webSocket that use to the connection between the webpage and the database
     * @param clientHandshake useless
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        v.add(webSocket);
        webSocket.send("Welcome board captain");
    }

    /**
     * That function is used to close a WebSocket in case of the operator quit the webpage.
     * @param webSocket that use to the connection between the webpage and the database
     * @param i useless
     * @param s useless
     * @param b useless
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        v.remove(webSocket);
        webSocket.close();
    }

    /**
     * This function is use to send a message to the entire webpage.
     * @param webSocket that use to the connection between the webpage and the database
     * @param s that contain "label"="value"
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        String message[] = s.split("=");

        if (message.length >= 2) {
            String label = message[0];
            String value = message[1];
            DataPoint dp = DataPoint.getDataPointFromLabel(label);
            if (dp != null) {
                dp.setValue(value);
            }
        }
        pushToWeb(message[0],message[1]);
    }

        @Override
    public void onError(WebSocket webSocket, Exception e) {

    }

    @Override
    public void onStart() {

    }

    /**
     * Controls if the Connector is the only one.
     * @return An unique DataBase
     */
    public static WebConnector getInstance() {
        if (wbc == null) {
            wbc = new WebConnector();
        }
        return wbc;
    }

    /**
     * Sends the label and value to the Web
     * @param label The name of the DataPoint
     * @param value The value of the DataPoint
     */
    private void pushToWeb(String label, String value) {
        for (WebSocket client:this.getConnections()) {
            if(client.isOpen()) {
                client.send(label + "=" + value);
                //System.out.println(label + "=" + value);
            }
        }
    }

    /**
     * Send the new value of the DataPoint to the Web
     * @param dp is a message that be use to send value in different location
     */
    @Override
    public void onNewValue(DataPoint dp) {
        pushToWeb(dp.getLabel(), dp.getValue());
    }

    // For the tests
    public static void main(String[] args) {
        WebConnector wc = new WebConnector();

    }
}


