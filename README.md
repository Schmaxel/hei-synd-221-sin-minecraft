# HEI-SYND-221-SIN-Minecraft
![](https://thumbs.dreamstime.com/b/minecraft-logo-online-game-dirt-block-illustrations-concept-design-isolated-186775550.jpg)

This project was made by Schmidhalter Axel and Héritier Daniel (Groupe 7) 


## Utility 

In the minecraft world, that we simulate the electrical grid, we have a wind turbine, a battery, solar panels and a coal factory for the producers and a house, a factory and a bunker as cunsommators.  

The goal is to send orders to the different intallation in Minecraft and at least to create a controller for the electrical grid.


## How to run it
For running the program, you have to download it and find the .jar file. It require that the META-INF and the ressources repositories are in the same folder as the .jar file. 

Open the dos and write this code : 

    cd <PathTo out\artifacts\Minecraft.jar>
    
    java -jar Minecraft.jar influx.sdi.hevs.ch SIn17 localhost 1502 -modbus4j



## Best score obtained without random condition

Our best score is 476'782 W


